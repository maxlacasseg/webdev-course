const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
// const bodyParser = require ('body-parser');
// const passport = require ('passport');
const logger = require('tracer').colorConsole();
// const mongoose = require ('mongoose');
// const morgan = require ('morgan');
const path = require('path');
// const fileUpload = require ('express-fileupload');
// const envResult = require ('dotenv').config ();

// if (envResult.error) {
//   logger.warn ({type: 'env', error: envResult.error});
// }

// //ADD DB CONNECTION TO REQUESTS
// const db = process.env.MONGO_URI;

//REQUIRE ROUTES
// const utilisateur = require ('./routes/Utilisateur');

// mongoose.connect (db, {useNewUrlParser: true, useUnifiedTopology: true }, err => {
//   if (err) return logger.log (err);
//   logger.info ('Connecté à la base de donnée');
// });

//PASSPORT INIT, ADDS TO REQUESTS
// passport.initialize ();
// require ('./config/passport') (passport);

//MIDDLEWARES
// app.use (
//   bodyParser.urlencoded ({
//     parameterLimit: 100000,
//     limit: '50mb',
//     extended: true,
//   })
// );
// app.use (bodyParser.json ());
// app.use (morgan ('dev'));
// app.use (fileUpload ());

//ROUTES
// app.use ('/api/utilisateur', utilisateur);

app.use(express.static(path.join(__dirname, "..", "client", "build")));
app.use(express.static(path.join(__dirname, "..", "client", "public")));
app.use('/tmp', express.static('../client/tmp'));

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '..', 'client', 'build', 'index.html'));
});

app.listen(port, err => {
    if (err) logger.log(err);
    logger.trace(`listening on ${port}`);
});